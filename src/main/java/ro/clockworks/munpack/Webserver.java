package ro.clockworks.munpack;

import io.javalin.Javalin;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Webserver {
    public static final Logger logger = LoggerFactory.getLogger(Webserver.class);
    public static final Pattern locationRegex = Pattern.compile("^[a-zA-Z0-9\\-_]+$");

    private final Main main;
    private final Javalin server;

    public Webserver(Main main) {
        this.main = main;
        server = Javalin.create();
        String route = main.getBaseurl() + "/accept";
        server.post(route, this::postZip);
        logger.info("Webserver configured on route {}", route);
    }

    public void start() {
        server.start(main.getHost(), main.getPort());
    }

    public void join() {
        try {
            server.server().server().getThreadPool().join();
        } catch (InterruptedException e) {
            logger.error("Error while joining webserver thread", e);
        }
    }

    private void postZip(Context context) {
        Path unpackLocation = main.getUnpackLocation();
        String authhdr = context.header("UnpackAuth");
        String unpackSubloc = context.header("UnpackLocation");
        if (authhdr == null || !authhdr.equals(main.getAuthHeader()) || unpackSubloc == null || !locationRegex.matcher(unpackSubloc).matches()) {
            context.status(400); // go to hell
            return;
        }
        if (!Files.exists(unpackLocation) || !Files.isDirectory(unpackLocation)) {
            logger.error("Unpack location is unreacheable");
            context.status(503); // server error
            return;
        }
        Path destination = unpackLocation.resolve(unpackSubloc);
        if (Files.exists(destination)) {
            if (!Files.isDirectory(destination)) {
                context.status(409); // conflict
                return;
            }
        } else {
            try {
                Files.createDirectory(destination);
            } catch (IOException exception) {
                logger.error("Failed to create destination directory", exception);
                context.status(503); // server error
                return;
            }
        }

        ZipInputStream zis = new ZipInputStream(context.bodyAsInputStream());
        try {
            for (ZipEntry entry = zis.getNextEntry(); entry != null; entry = zis.getNextEntry()) {
                if (entry.isDirectory() || entry.getName().contains("/")) {
                    continue; // ignore directories
                }
                String filename = entry.getName();
                Path writePath = destination.resolve(filename);
                int len;
                OutputStream fos = Files.newOutputStream(writePath);
                byte[] buffer = new byte[4096];
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
            }
            zis.closeEntry();
            zis.close();
            logger.info("Successfully unpacked minute");
        } catch (IOException ioe) {
            logger.error("Error unzipping file: {}", ioe.getMessage());
            context.status(503);
            return;
        }

    }

}
