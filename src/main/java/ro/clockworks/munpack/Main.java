package ro.clockworks.munpack;

import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Callable;

@CommandLine.Command(name = "MinuteUnpacker", version = "MinuteUnpacker 1.1", mixinStandardHelpOptions = true)
public class Main implements Callable<Integer> {
    public static final Logger logger = LoggerFactory.getLogger(Main.class);

    @CommandLine.Option(names = {"--host"}, description = "Server address to listen on")
    @Getter
    private String host = "127.0.0.1";

    @CommandLine.Option(names = {"--port"}, description = "Server port to listen on")
    @Getter
    private int port = 4554;

    @CommandLine.Option(names = {"--unpack-location"}, description = "Location to unpack minutes", required = true)
    private String unpackLocationString;

    @CommandLine.Option(names = {"--baseurl"}, description = "Base URL for HTTP calls")
    @Getter
    private String baseurl = "/";

    @CommandLine.Option(names = {"--auth"}, description = "UnpackAuth header token used for authentication", required = true)
    @Getter
    private String authHeader;

    @Getter
    private Path unpackLocation;

    @Override
    public Integer call() throws Exception {
        unpackLocation = Path.of(unpackLocationString);
        if (!Files.exists(unpackLocation) || !Files.isDirectory(unpackLocation)) {
            logger.error("Unpacking location {} does not exist or is not a folder", unpackLocation);
            return -1;
        }
        if (!baseurl.startsWith("/")) {
            logger.error("Base URL does not start with a leading slash");
        }
        while (baseurl.endsWith("/")) {
            baseurl = baseurl.substring(0, baseurl.length() - 1);
        }

        Webserver webserver = new Webserver(this);
        webserver.start();
        webserver.join();
        return 0;
    }

    public static void main(String[] args) {
        int exitCode = new CommandLine(new Main()).execute(args);
        System.exit(exitCode);
    }
}
